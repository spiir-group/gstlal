#!/usr/bin/env python3
#
# Copyright (C)
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

"""Set the GPS time at which to remove count tracker counts from"""


#
# =============================================================================
#
#                                   Preamble
#
# =============================================================================
#


from optparse import OptionParser
import urllib.parse
import urllib.request
import requests
from requests.exceptions import ConnectionError
from urllib.error import URLError
import json
import logging


#
# =============================================================================
#
#                                 Command Line
#
# =============================================================================
#


def parse_command_line():
	parser = OptionParser(
		usage = "%prog [options] registry1.txt ...",
		description = __doc__
	)
	parser.add_option("--gps-time", metavar = "seconds", type = "int", action = "append", help = "Set the gps time at which to remove count tracker counts from")
	parser.add_option("--action", type = "choice", choices=["remove", "check", "undo-remove"], help = "Choose an action from ['remove', 'check', 'undo-remove'. (remove : Remove counts at the given time, check : Check whether the given time has alread been sent successfully, undo-remove : Undo the removed counts at the given time)")
	parser.add_option("-v", "--verbose", action = "store_true", help = "Be verbose (optional).")

	options, filenames = parser.parse_args()

	if len(filenames) < 1:
		raise ValueError("must provide the name of at least one registry file")

	return options, filenames


#
# =============================================================================
#
#                                     Main
#
# =============================================================================
#


#
# parse command line
#


options, filenames = parse_command_line()


def get_url(filename):
	return ["%sremove_counts.txt" % url.strip() for url in open(filename)][0]


def check(filename, gps_time):
	url = get_url(filename)
	x = requests.get(url)
	return int(gps_time) in json.loads(x.text)


def undo(filename, gps_time):
	url = get_url(filename)
	post_data = urllib.parse.urlencode({"gps_time_undo": gps_time}).encode('utf-8')
	return urllib.request.urlopen(url, data = post_data)


def submit(filename, gps_time):
	url = get_url(filename)
	post_data = urllib.parse.urlencode({"gps_time": gps_time}).encode('utf-8')
	return urllib.request.urlopen(url, data = post_data)

logger = logging.getLogger('gstlal_ll_inspiral_remove_counts')
log_level = logging.INFO if options.verbose else logging.WARN
logger.setLevel(log_level)


if options.action == "remove":
	post_func = submit
	check_bool = True
elif options.action == "undo-remove":
	post_func = undo
	check_bool = False
partially_OK = False
all_OK = True
for filename in filenames:
	for gps_time in options.gps_time:
		if options.action == "check":
			try:
				if not check(filename, gps_time):
					all_OK &= False
					logger.warning(f'gps-time {gps_time} not in {filename}')
				else:
					partially_OK |= True
					logger.info(f'gps-time {gps_time} found in {filename}')
			except ConnectionError:
				logger.error(f'ConnectionError : could not check for gps-time {gps_time} in {filename}')
				all_OK &= False
		else:
			try:
				response = post_func(filename, gps_time)
				msg = response.read().decode("utf-8")
				if "OK" in msg and check(filename, gps_time) == check_bool:
					partially_OK |= True
					logger.info(msg.strip() + f'for {filename}')
					continue
				elif "OK" in msg:
					logger.warning(f'gps-time {gps_time} has been {post_func.__name__}-ed but failed for {filename}')
				elif "ValueError" in msg:
					raise ValueError(msg.lstrip("ValueError : "))
				elif "warning" in msg:
					logger.warning(msg)
				elif "error" in msg:
					logger.error(f'Error came up while {post_func.__name__}-ing for gps-time {gps_time} and {filename}')
			except URLError:
				logger.error(f'URLError : calling {post_func.__name__ }() for gps-time {gps_time} and {filename} failed')
			all_OK &= False

if not partially_OK:
    logger.warning(f'"{options.action}" is getting either warning or error for all the given files. Please check if the gps time and the files are valid.')
if all_OK:
	logger.warning(f'"{options.action}" is properly done for all the given files.')
