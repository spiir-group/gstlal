#!/usr/bin/env python3
#
# Copyright (C) 2018, 2019 Chad Hanna
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import sys
import numpy
import argparse
from gstlal.stats.inspiral_extrinsics import TimePhaseSNR

parser = argparse.ArgumentParser(description = 'combine dt dphi snr ratio pdfs')
parser.add_argument("--output", metavar = 'filename', type = str, help = 'The output file name', default = "inspiral_dtdphi_pdf.h5")
parser.add_argument('files', metavar = 'filename', type = str, nargs='+', help = 'The input pdf filenames')
options = parser.parse_args()

# Read in and combine all of the input files
files = options.files
TPS = TimePhaseSNR.from_hdf5(files[0], files[1:])

# compute the normalization
time, phase, deff = TimePhaseSNR.tile(NSIDE = 8, NANGLE = 17)
# This actually doesn't matter it is just needed to map from eff distance to
# snr, but the TimePhaseSNR code actually undoes this...
h = {"H1":1., "L1":1., "V1":1., "K1":1.}

# set the norm to 1
combos = TPS.combos + (("H1",),("L1",),("V1",),("K1",))
norm = dict((frozenset(k), 1.) for k in combos)
TPS.norm = norm

def extract_dict(DICT, keys):
	return dict((k,v) for k,v in DICT.items() if k in keys)

norm = dict((frozenset(k), 0.) for k in combos)
for i in range(len(time["H1"])):
	t = dict((k, v[i]) for k, v in time.items())
	p = dict((k, v[i]) for k, v in phase.items())
	s = dict((k, h[k] / v[i]) for k, v in deff.items())

	for ifos in combos:
		t2 = extract_dict(t, ifos)
		p2 = extract_dict(p, ifos)
		s2 = extract_dict(s, ifos)
		h2 = extract_dict(h, ifos)
		snr_net = sum(x**2 for x in s2.values())**.5
		norm[frozenset(ifos)] += TPS(t2,p2,s2,h2) * snr_net**4 / len(time["H1"]) * 1. / (16. * numpy.pi**2) / (snr_net / numpy.prod([snr for snr in s2.values()]))

TPS.norm = norm

# write the result to disk
TPS.to_hdf5(options.output)
