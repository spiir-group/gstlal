SHELL := /bin/bash -O extglob
{% if config.filter.injections.file %}

#Set times for injection sets
START = $(shell lalapps_tconvert)
DT = $(shell echo 1209600) #2weeks
STOP = $(shell echo $$(( $(START) + $(DT) )))
{% endif %}

{% if config.dcc %}
DCC={{ config.dcc.document_number}}
VERSION={{ config.dcc.version}}
{% endif %}

.PHONY: help
help :
	@echo 'Commands for online analysis:'
	@echo
	@echo '  make env             generate environment (env.sh) to source'
	@echo '  make setup           create and launch a DAG to generate prerequisite data products'
{% if config.filter.injections.file %}
	@echo '  make injection_dag   create and launch lvc rates DAG to generate injections'
	@echo '  make injections      combine injections from DAG into a single file'
{% endif %}
	@echo '  make dag             generate the online DAG for condor submission'
	@echo '  make launch          launch online analysis via condor'
	@echo '  make condor_id       display the condor ID of the analysis DAG'
	@echo '  make archive         make an archive of the current analysis and start a new one'
	@echo '  make clean           clean generated files'
	@echo

.PHONY: env
env : env.sh

env.sh :
	@echo 'export SINGULARITY_PWD=$(PWD)' >> env.sh
	@echo 'export SINGULARITY_BIND=$${LAL_DATA_PATH}' >> env.sh
{% if config.condor.proxy %}
	@echo 'export X509_USER_PROXY={{ config.condor.proxy }}' >> env.sh
{% elif config.condor.certificate %}
	@echo 'unset X509_USER_PROXY' >> env.sh
	@echo 'export X509_USER_CERT={{ config.condor.certificate }}' >> env.sh
	@echo 'export X509_USER_KEY={{ config.condor.certificate }}' >> env.sh
{% endif %}
	@echo 'export GSTLAL_FIR_WHITEN=0' >> env.sh
	@echo 'export PYTHONUNBUFFERED=1' >> env.sh
	@echo 'export HDF5_USE_FILE_LOCKING='"'"'FALSE'"'" >> env.sh
	@echo '. ./influx_creds.sh' >> env.sh
	@echo 'generated environment script: env.sh'

.PHONY: enter
enter :
	singularity run -B ${TMPDIR},${LAL_DATA_PATH},/cvmfs,/var/lib/condor,/var/run/condor {{ config.condor.singularity_image }}

.PHONY: condor_id
condor_id :
	condor_q | grep -w online_inspiral_dag | awk '{print $$2}' | cut -d'+' -f2

.PHONY: archive
archive :
	mv run archive/run.$(shell date +'%y.%m.%d')
	mkdir -p run
	cp archive/run.$(shell date +'%y.%m.%d')/Makefile run

.PHONY: launch
launch : online_inspiral_{{ config.tag }}.dag
	condor_submit_dag -f $^
	@echo ""

PHONY: launch-setup
launch-setup : online_setup_{{ config.tag }}.dag
	condor_submit_dag $^
	@echo ""

.PHONY: dag
dag : online_inspiral_{{ config.tag }}.dag

.PHONY: setup
setup : online_setup_{{ config.tag }}.dag

{% if config.pastro %}
online_%_{{ config.tag }}.dag : $(HOME)/.config/gstlal/{{ config.condor.profile }}.yml tisi.xml {{ config.data.template_bank}} {{ config.svd.manifest }} {{ config.prior.mass_model }} {% for key in config.pastro.keys() %} {{ config.pastro[key].mass_model }} {% endfor %} {{ config.data.reference_psd }} {% if config.snr_optimizer %} {{ config.snr_optimizer.seed_bank }} {% endif %}

{% else %}
online_%_{{ config.tag }}.dag : {{ config.svd.manifest }} $(HOME)/.config/gstlal/{{ config.condor.profile }}.yml tisi.xml {{ config.data.template_bank}} {{ config.prior.mass_model }} {{ config.data.reference_psd }} {% if config.snr_optimizer %} {{ config.snr_optimizer.seed_bank }} {% endif %}

{% endif %}
	gstlal_ll_inspiral_workflow create -c config.yml --workflow $*
	@echo ""

{% if config.dcc %}
.PHONY: dcc-files
dcc-files : {{ config.data.template_bank }} {{ config.prior.mass_model }} {% for key in config.pastro.keys() %} {{ config.pastro[key].mass_model }} {% endfor %} {{ config.data.reference_psd }} {% if config.snr_optimizer %} {{ config.snr_optimizer.seed_bank }} {% endif %}

	@echo ""

{{ config.data.template_bank }} : {{ config.data.template_bank | replace("bank/", "$(DCC)/$(DCC)-$(VERSION)/") }}
	mv $< $@
	cd $(DCC)/$(DCC)-$(VERSION)/ && ln -s $(PWD)/{{ config.data.template_bank }} ./

{{ config.prior.mass_model }} : {{ config.prior.mass_model | replace("mass_model/", "$(DCC)/$(DCC)-$(VERSION)/") }}
	mv $< $@
	cd $(DCC)/$(DCC)-$(VERSION)/ && ln -s $(PWD)/{{ config.prior.mass_model }} ./
{% for key in config.pastro.keys() %}

{{ config.pastro[key].mass_model }} : {{ config.pastro[key].mass_model | replace("mass_model/", "$(DCC)/$(DCC)-$(VERSION)/") }}
	mv $< $@
	cd $(DCC)/$(DCC)-$(VERSION)/ && ln -s $(PWD)/{{ config.pastro[key].mass_model }} ./
{% endfor %}

{{ config.data.reference_psd }} : {{ config.data.reference_psd | replace("psd/", "$(DCC)/$(DCC)-$(VERSION)/") }}
	mv $< $@
	cd $(DCC)/$(DCC)-$(VERSION)/ && ln -s $(PWD)/{{ config.data.reference_psd }} ./

{% if config.snr_optimizer %}
{{ config.snr_optimizer.seed_bank }} : {{ config.snr_optimizer.seed_bank | replace("bank/", "$(DCC)/$(DCC)-$(VERSION)/") }}
	mv $< $@
	cd $(DCC)/$(DCC)-$(VERSION)/ && ln -s $(PWD)/{{ config.snr_optimizer.seed_bank }} ./
{% endif %}


$(DCC)/$(DCC)-$(VERSION)/% :
	@echo 'Type "y" when downloading the following files'
	@echo '{{ config.data.template_bank | replace("bank/", "") }}'
	@echo '{{ config.prior.mass_model | replace("mass_model/", "") }}'
	{% for key in config.pastro.keys() %}
	@echo '{{ config.pastro[key].mass_model | replace("mass_model/", "") }}'
	{% endfor %}
	{% if config.snr_optimizer %}
	@echo '{{ config.snr_optimizer.seed_bank | replace("bank/", "")}}'
	{% endif %}
	. /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/etc/profile.d/conda.sh &&\
	conda activate igwn &&\
	dcc archive --archive-dir=. --files -i --public $(DCC)-$(VERSION) &&\
	conda deactivate
	@echo ""
{% endif %}

{% if config.filter.injections.file %}

.PHONY: injections injection_dag
injections: {% for inj_name, params in config.filter.injections.file.items() %} {{ params.file }}{% endfor %}

injection_dag: {% for inj_name, params in config.filter.injections.file.items() %} {{ inj_name }}_inj_dag{% endfor %}

injections/%.xml.gz:
	if [ -f injections/$*.xml.gz ]; then mv injections/$*.xml.gz injections/archive/$*-_-$(START).xml.gz; fi
	cd injections && rm -f rates_injections_$*.*
	cd injections && rm -rf logs
	cd injections && ligolw_add $*-*-*.xml.gz -o $*.xml.gz
	cd injections && rm $*-*-*.xml.gz
	@echo ""

%_inj_dag:
	mkdir -p injections/archive
	cd injections && python3 make_ini_file.py ${START}
	cd injections && python3 lvc_rates_injection_dag \
		--tmp-dir=. \
		--tag=$* \
		--analysis-ini=analysis.ini \
		--$*-params-ini=$*_params.ini \
		--condor-command=accounting_group={{ config.condor.accounting_group }} \
		--condor-command=accounting_group_user={{ config.condor.accounting_group_user }}

print_vt_injected:
	ligolw_print injections/bbh.xml.gz -t process_params -c param -c value -d " "|grep "VT"|awk '{print $$2}'>vt.txt
	ligolw_print injections/bbh.xml.gz -t process_params -c param -c value -d " "|grep "acceptance-rate"|awk '{print $$2}'>rate.txt
	paste vt.txt rate.txt|awk '{print $$1*$$2}'|awk '{SUM+=$$1}END{print SUM}'
	rm vt.txt rate.txt
{% endif %}

tisi.xml :
	lalburst_gen_timeslides {% for instrument in config.ifos %}--instrument={{ instrument }}=0:0:0 {% endfor %}$@
	@echo ""

zerolag_ranking_stat :
	mkdir -p $@
	@echo ""

# Produce split banks
{% if config.data.template_bank is mapping %}
{{ config.svd.manifest }} :{% for bank_file in config.data.template_bank.values() %} {{ bank_file }}{% endfor %}
{% else %}
{{ config.svd.manifest }} : {{ config.data.template_bank }}
{% endif %}

	mkdir -p svd_bank
	mkdir -p split_bank
    {% if config.svd.sub_banks %}
	{% for bank_name, params in config.svd.sub_banks.items() %}
	gstlal_inspiral_bank_splitter \
		--f-low {{ params.f_low }} \
		{% if params.num_mu_bins %}
		--group-by-mu {{ params.num_mu_bins }} \
		{% else %}
		--group-by-chi {{ params.num_chi_bins }} \
		{% endif %}
		--output-path split_bank \
		{% for approx in config.svd.approximant %}
		--approximant {{ approx }} \
		{% endfor %}
		--overlap {{ params.overlap }} \
		--instrument {% for instrument in config.ifos %}{{ instrument }}{% endfor %} \
		--n {{ params.num_split_templates }} \
		{% if params.sort_by %}
		--sort-by {{ params.sort_by }} \
		{% else %}
		--sort-by {{ config.svd.sort_by }}  \
		{% endif %}
		--f-final {{ params.max_f_final }} \
		--num-banks {{ params.num_banks }} \
		--stats-file $@ \
		--bank-name {{ bank_name }} \
		{{ config.data.template_bank[bank_name] }}
	{% endfor %}
	{% else %}
	gstlal_inspiral_bank_splitter \
		--f-low {{ config.svd.f_low }} \
		{% if config.svd.num_mu_bins %}
		--group-by-mu {{ config.svd.num_mu_bins }} \
			{% else %}
		--group-by-chi {{ config.svd.num_chi_bins }} \
		{% endif %}
		--output-path split_bank \
		{% for approx in config.svd.approximant %}
		--approximant {{ approx }} \
		{% endfor %}
		--overlap {{ config.svd.overlap }} \
		--instrument {% for instrument in config.ifos %}{{ instrument }}{% endfor %} \
		--n {{ config.svd.num_split_templates }} \
		--sort-by {{ config.svd.sort_by }}  \
		--f-final {{ config.svd.max_f_final }} \
		--num-banks {{ config.svd.num_banks }} \
		--stats-file $@ \
		$<
	{% endif %}
	@echo ""

set-far-thresh :
	gstlal_ll_inspiral_gracedb_threshold \
		--gracedb-far-threshold 2.78e-4 \
		0*registry.txt

get-far-thresh :
	gstlal_ll_inspiral_gracedb_threshold \
		0*registry.txt

set-high-far-thresh :
	gstlal_ll_inspiral_gracedb_threshold \
		--gracedb-far-threshold 1e-8 \
		0*registry.txt

disable-uploads :
	gstlal_ll_inspiral_gracedb_threshold \
		--gracedb-far-threshold -1 \
		0*registry.txt

clean :
	rm -rf online_*_dag.* *.sub logs
	rm -rf *_registry.txt 1*
	rm -rf split_bank filter dist_stats zerolag_dist_stats zerolag_dist_stat_pdfs dist_stat_pdfs
	rm -rf aggregator
	rm -rf env.sh tisi.xml *-GSTLAL_SVD_MANIFEST*-0-0.json
	rm -rf $(DCC)/
{% if config.filter.injections.file %}
	rm -rf injections/archive/
{% endif %}
