all : dag

.PHONY: launch
launch : {{ workflow }}_inspiral_dag.dag
	condor_submit_dag $<

.PHONY: dag
dag : {{ workflow }}_inspiral_dag.dag
	@echo ""

.PHONY: summary
summary :
	mkdir -p {{ config.summary.webdir }}
	gstlal_inspiral_summary_page \
		--title gstlal-{{ config.start }}-{{ config.stop }}-closed-box \
		--webserver-dir {{ config.summary.webdir }} \
		--output-user-tag ALL_COMBINED \
		--output-user-tag PRECESSION_COMBINED \
		{% for inj_name in config.filter.injections.keys() %}
		--output-user-tag {{ inj_name.upper() }}_INJECTION \
		--output-user-tag {{ inj_name.upper() }}_INJECTION_PRECESSION \
		{% endfor %}
		--glob-path plots
	mkdir -p {{ config.summary.webdir }}/OPEN-BOX
	gstlal_inspiral_summary_page \
		--title gstlal-{{ config.start }}-{{ config.stop }}-open-box \
		--webserver-dir {{ config.summary.webdir }}/OPEN-BOX \
		--output-user-tag ALL_COMBINED \
		--output-user-tag PRECESSION_COMBINED \
		{% for inj_name in config.filter.injections.keys() %}
		--output-user-tag {{ inj_name.upper() }}_INJECTION \
		--output-user-tag {{ inj_name.upper() }}_INJECTION_PRECESSION \
		{% endfor %}
		--open-box \
		--glob-path plots
	chmod a-r {{ config.summary.webdir }}/OPEN-BOX

.PHONY: unlock
unlock :
	chmod a+r {{ config.summary.webdir }}/OPEN-BOX

{% if config.segments.backend == 'gwosc' %}
segments.xml.gz : CAT1_vetoes.xml.gz
	gstlal_query_gwosc_segments -o $@ {{ config.start }} {{ config.stop }}{% for instrument in config.ifos %} {{ instrument }}{% endfor %}

	gstlal_segments_operations --segment-name vetoes --output-segment-name datasegments --union --output-file CAT1_vetoes_renamed.xml.gz $< $<
	gstlal_segments_operations --diff --output-file $@ $@ CAT1_vetoes_renamed.xml.gz
	gstlal_segments_trim --trim 0 --gps-start-time {{ config.start }} --gps-end-time {{ config.stop }} --min-length 512 --output $@ $@
	rm CAT1_vetoes_renamed.xml.gz
	@echo ""

vetoes.xml.gz : {{ config.segments.vetoes.category }}_vetoes.xml.gz
	cp $< $@
	@echo ""

{{ config.segments.vetoes.category }}_vetoes.xml.gz :
	gstlal_query_gwosc_veto_segments -o $@ {{ config.start }} {{ config.stop }} {% for instrument in config.ifos %} {{ instrument }}{% endfor %} --category {{ config.segments.vetoes.category }} --cumulative
	@echo ""
{% elif config.segments.backend == 'dqsegdb' %}
segments.xml.gz : CAT1_vetoes.xml.gz
	gstlal_query_dqsegdb_segments -o $@ {{ config.start }} {{ config.stop }}{% for instrument in config.ifos %} {{ instrument }}{% endfor %} {% for instrument, flag in config.segments.science.items() %} -f {{ "{}:{}".format(instrument, flag) }}{% endfor %}

	gstlal_segments_operations --segment-name vetoes --output-segment-name datasegments --union --output-file CAT1_vetoes_renamed.xml.gz $< $<
	gstlal_segments_operations --diff --output-file $@ $@ CAT1_vetoes_renamed.xml.gz
	gstlal_segments_trim --trim 0 --gps-start-time {{ config.start }} --gps-end-time {{ config.stop }} --min-length 512 --output $@ $@
	rm CAT1_vetoes_renamed.xml.gz
	@echo ""

vetoes.xml.gz : {{ config.segments.vetoes.category }}_vetoes.xml.gz
	cp $< $@
	@echo ""

{{ config.segments.vetoes.category }}_vetoes.xml.gz : {{ config.segments.vetoes.veto_definer.file }}
	gstlal_query_dqsegdb_veto_segments -o $@ {{ config.start }} {{ config.stop }} {% for instrument in config.ifos %} {{ instrument }}{% endfor %} --category {{ config.segments.vetoes.category }} --cumulative --veto-definer-file $<
	@echo ""

{{ config.segments.vetoes.veto_definer.file }} :
	git archive --remote=git@git.ligo.org:detchar/veto-definitions.git {{ config.segments.vetoes.veto_definer.version }}:cbc/{{ config.segments.vetoes.veto_definer.epoch }} $@ | tar -x
	ligolw_no_ilwdchar $@

{% endif %}

tisi.xml : inj_tisi.xml
	lalburst_gen_timeslides {% for instrument, slides in config.filter.time_slides.items() %} --instrument={{ instrument }}={{ slides }}{% endfor %} bg_tisi.xml
	ligolw_add --output $@ bg_tisi.xml $<
	@echo ""

inj_tisi.xml :
	lalburst_gen_timeslides {% for instrument in config.ifos %} --instrument={{ instrument }}=0:0:0{% endfor %} $@
	@echo ""

{% if config.data.template_bank is mapping %}
{{ config.svd.manifest }} :{% for bank_file in config.data.template_bank.values() %} {{ bank_file }}{% endfor %}
{% else %}
{{ config.svd.manifest }} : {{ config.data.template_bank }}
{% endif %}

	mkdir -p split_bank
    {% if config.svd.sub_banks %}
	{% for bank_name, params in config.svd.sub_banks.items() %}
	gstlal_inspiral_bank_splitter \
		--f-low {{ params.f_low }} \
		{% if params.num_mu_bins %}
		--group-by-mu {{ params.num_mu_bins }} \
		{% else %}
		--group-by-chi {{ params.num_chi_bins }} \
		{% endif %}
		--output-path split_bank \
		{% for approx in config.svd.approximant %}
		--approximant {{ approx }} \
		{% endfor %}
		--overlap {{ params.overlap }} \
		--instrument {% for instrument in config.ifos %}{{ instrument }}{% endfor %} \
		--n {{ params.num_split_templates }} \
		{% if params.sort_by %}
		--sort-by {{ params.sort_by }} \
		{% else %}
		--sort-by {{ config.svd.sort_by }}  \
		{% endif %}
		--f-final {{ config.svd.max_f_final }} \
		--num-banks {{ params.num_banks }} \
		--stats-file $@ \
		--bank-name {{ bank_name }} \
		{{ config.data.template_bank[bank_name] }}
	{% endfor %}
	{% else %}
	gstlal_inspiral_bank_splitter \
		--f-low {{ config.svd.f_low }} \
		{% if config.svd.num_mu_bins %}
		--group-by-mu {{ config.svd.num_mu_bins }} \
			{% else %}
		--group-by-chi {{ config.svd.num_chi_bins }} \
		{% endif %}
		--output-path split_bank \
		{% for approx in config.svd.approximant %}
		--approximant {{ approx }} \
		{% endfor %}
		--overlap {{ config.svd.overlap }} \
		--instrument {% for instrument in config.ifos %}{{ instrument }}{% endfor %} \
		--n {{ config.svd.num_split_templates }} \
		--sort-by {{ config.svd.sort_by }}  \
		--f-final {{ config.svd.max_f_final }} \
		--num-banks {{ config.svd.num_banks }} \
		--stats-file $@ \
		$<
	{% endif %}
	@echo ""

%_inspiral_dag.dag : {{ config.svd.manifest }} vetoes.xml.gz segments.xml.gz tisi.xml plots {% for inj in config.filter.injections.values() %} {{ inj.file }}{% endfor %}

	gstlal_inspiral_workflow create -c config.yml --workflow $*

{% if config.injections.sets %}
{% for inj_name, params in config.injections.sets.items() %}
{{ params.file }} :
	lalapps_inspinj \
		--gps-start-time {{ config.start + params.time.shift }} \
		--gps-end-time {{ config.stop }} \
		--enable-spin \
		{% if 'spin_aligned' in params %}
		--aligned \
		{% endif %}
		--i-distr uniform \
		--l-distr random \
		--t-distr uniform \
		--dchirp-distr uniform \
		--m-distr {{ params.mass_distr }} \
		{% for param in ['mass1', 'mass2', 'spin1', 'spin2', 'distance'] %}
		{% for stat, val in params[param].items() %}
		--{{ stat }}-{{ param }} {{ val }} \
		{% endfor %}
		{% endfor %}
		--f-lower {{ params.f_low }} \
		--waveform {{ params.waveform }} \
		--time-step {{ params.time.step }} \
		--time-interval {{ params.time.interval }} \
		--taper-injection startend \
		--seed {{ params.seed }} \
		--output $@
	ligolw_no_ilwdchar $@
	@echo ""

{% endfor %}
{% endif %}
{% if config.injections.combine %}
{{ config.injections.combined_file }} : {% for inj in config.injections.sets.values() %} {{ inj.file }}{% endfor %}

	gstlal_inspiral_combine_injection_sets $^ --single-output -o $(basename {{ config.injections.combined_file }})
	rm injection_str.txt
{% endif %}

plots :
	mkdir -p $@

clean : 
	rm -rf segments.xml.gz *tisi.xml
	rm -rf split_bank *vetoes.xml.gz {{ config.svd.manifest }}
	rm -rf reference_psd median_psd
	rm -rf filter rank plots
	rm -rf logs *inspiral_dag.dag* *inspiral_dag.sh *.sub _condor_stdout
	rm -rf {% for inj in config.filter.injections.values() %} {{ inj.file }}{% endfor %}

	{% if config.injections.sets %}
	rm -rf {% for inj in config.injections.sets.values() %} {{ inj.file }}{% endfor %}
	{% endif %}


clean-lite :
	rm -rf logs/* *inspiral_dag.dag* *inspiral_dag.sh *.sub
