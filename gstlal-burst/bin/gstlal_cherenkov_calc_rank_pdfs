#!/usr/bin/env python3
#
# Copyright (C) 2010--2015  Kipp Cannon, Chad Hanna
# Copyright (C) 2021  Soichiro Kuwawhara
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

### A program to compute the noise probability distributions of likehood ratios for inspiral triggers

#
# =============================================================================
#
#                                   Preamble
#
# =============================================================================
#


from optparse import OptionParser

from ligo.lw import ligolw
from ligo.lw import utils as ligolw_utils
from ligo.lw.utils import process as ligolw_process
from lal.utils import CacheEntry

from gstlal.cherenkov import rankingstat as cherenkov_rankingstat

__author__ = "Soichiro Kuwara <soichiro.kuwahara@ligo.org>"


#
# =============================================================================
#
#                                 Command Line
#
# =============================================================================
#


def parse_command_line():
	parser = OptionParser(
		description = "Rankngstat calculation program for Cherenkov burst search.",
		usage = "%prog [options] [rankingstatxml ...]"
	)
	parser.add_option("--output", metavar = "filename", help = "Write ranking statistic PDFs to this LIGO Light-Weight XML file.")
	parser.add_option("--ranking-stat-cache", metavar = "filename", help = "Also load the ranking statistic likelihood ratio data files listed in this LAL cache.  See lalapps_path2cache for information on how to produce a LAL cache file.")
	parser.add_option("--ranking-stat-samples", metavar = "N", default = 2**24, type = "int", help = "Construct ranking statistic histograms by drawing this many samples from the ranking statistic generator (default = 2^24).")
	parser.add_option("-v", "--verbose", action = "store_true", help = "Be verbose.")
	options, urls = parser.parse_args()

	paramdict = options.__dict__.copy()

	if options.ranking_stat_cache is not None:
		urls += [CacheEntry(line).url for line in open(options.ranking_stat_cache)]
	if not urls:
		raise ValueError("must provide some ranking statistic files")

	if options.output is None:
		raise ValueError("must set --output")

	return options, urls, paramdict


#
# =============================================================================
#
#                                     Main
#
# =============================================================================
#


#
# command line
#


options, urls, paramdict = parse_command_line()


#
# start the output document and record our start time
#


xmldoc = ligolw.Document()
xmldoc.appendChild(ligolw.LIGO_LW())


#
# load parameter distribution data
#


rankingstat = cherenkov_rankingstat.marginalize_rankingstat_urls(urls, verbose = options.verbose)
process = ligolw_process.register_to_xmldoc(xmldoc, "gstlal_cherenkov_calc_rank_pdfs", paramdict = paramdict, instruments = rankingstat.instruments)


#
# invoke .finish() to apply density estimation kernels and correct the
# normalization.
#


rankingstat.finish()


#
# generate likelihood ratio histograms
#


rankingstatpdf = cherenkov_rankingstat.RankingStatPDF(rankingstat, nsamples = options.ranking_stat_samples, verbose = options.verbose)


#
# Write the ranking statistic distribution data to a file
#


xmldoc.childNodes[-1].appendChild(rankingstatpdf.to_xml("gstlal_cherenkov_rankingstat_pdf"))
process.set_end_time_now()
ligolw_utils.write_filename(xmldoc, options.output, verbose = options.verbose)
