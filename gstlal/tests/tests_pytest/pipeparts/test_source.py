"""Unittests for source pipeparts"""
import pytest
from lal import LIGOTimeGPS

from gstlal import gstpipetools
from gstlal.pipeparts import pipetools, source
from gstlal.pipeparts.pipetools import Gst, GObject
from gstlal.utilities import testtools


def assert_elem_props(elem, name):
	assert gstpipetools.is_element(elem)
	assert type(elem).__name__ == name


class TestSource:
	"""Group test for sources"""

	@pytest.fixture(scope='class', autouse=True)
	def pipeline(self):
		GObject.MainLoop()
		return Gst.Pipeline('TestEncode')

	@pytest.fixture(scope='class', autouse=True)
	def src(self, pipeline):
		return pipetools.make_element_with_src(pipeline, None, 'fakesrc')

	def test_cache(self, pipeline, src):
		"""Test cache source"""
		elem = source.cache(pipeline, 'cache-location.txt')
		assert_elem_props(elem, 'GstLALCacheSrc')

	@testtools.requires_full_build
	def test_lvshm(self, pipeline, src):
		"""Test lvshm"""
		elem = source.lvshm(pipeline, 'LHO_Data')
		assert_elem_props(elem, 'GDSLVSHMSrc')

	@testtools.requires_full_build
	def test_frame_x_mit(self, pipeline, src):
		"""Test frame_x_mit"""
		elem = source.framexmit(pipeline)
		assert_elem_props(elem, 'GstGDSFramexmitSrc')

	@testtools.requires_full_build
	def test_nds(self, pipeline, src):
		"""Test nds source"""
		elem = source.nds(pipeline, 'host', 'inst', 'channel',
						  channel_type=source.NDSChannelType.TestPoint)
		assert_elem_props(elem, 'GSTLALNDSSrc')

	def test_audio_test(self, pipeline, src):
		"""Test audio test source"""
		elem = source.audio_test(pipeline)
		assert_elem_props(elem, 'GstAudioTestSrc')

	def test_fake(self, pipeline, src):
		"""Test fake source"""
		elem = source.fake(pipeline, 'H1', 'channel')
		assert_elem_props(elem, 'GstTagInject')

	def test_segment(self, pipeline, src):
		"""Test segment source"""
		elem = source.segment(pipeline, [(LIGOTimeGPS(123456), LIGOTimeGPS(123457))])
		assert_elem_props(elem, 'GSTLALSegmentSrc')

	@testtools.impl_deprecated
	def test_fake_ligo(self, pipeline, src):
		"""Test fake ligo source"""
		elem = source.fake_ligo(pipeline)
		assert_elem_props(elem, 'lal_fakeligosrc')

	@testtools.impl_deprecated
	def test_fake_aligo(self, pipeline, src):
		"""Test fake aligo source"""
		elem = source.fake_aligo(pipeline)
		assert_elem_props(elem, 'lal_fakeadvligosrc')

	@testtools.impl_deprecated
	def test_fake_avirgo(self, pipeline, src):
		"""Test fake avirgo source"""
		elem = source.fake_avirgo(pipeline)
		assert_elem_props(elem, 'lal_fakeadvvirgosrc')

