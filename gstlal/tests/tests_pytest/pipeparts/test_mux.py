"""Unittests for mux pipeparts"""
import pytest

from gstlal import gstpipetools
from gstlal.pipeparts import mux, pipetools
from gstlal.pipeparts.pipetools import Gst, GObject
from gstlal.utilities import testtools


def assert_elem_props(elem, name):
	assert gstpipetools.is_element(elem)
	assert type(elem).__name__ == name


class TestMux:
	"""Test mux pipeparts"""

	@pytest.fixture(scope='class', autouse=True)
	def pipeline(self):
		GObject.MainLoop()
		return Gst.Pipeline('TestEncode')

	@pytest.fixture(scope='class', autouse=True)
	def src(self, pipeline):
		return pipetools.make_element_with_src(pipeline, None, 'fakesrc')

	@testtools.requires_full_build
	def test_framecpp_demux(self, pipeline, src):
		"""Test framecpp demux"""
		elem = mux.framecpp_channel_demux(pipeline, src)
		assert_elem_props(elem, "GstFrameCPPChannelDemux")

	@testtools.requires_full_build
	def test_framecpp_mux(self, pipeline, src):
		"""Test framecpp demux"""
		elem = mux.framecpp_channel_mux(pipeline, None)
		assert_elem_props(elem, "GstFrameCPPChannelMux")

	@testtools.broken('oggmux not available in current version of Gst')
	def test_ogg(self, pipeline, src):
		"""Test ogg mux"""
		elem = mux.ogg_mux(pipeline, src)
		assert_elem_props(elem, "GstOggMux")
