"""Unittests for trigger pipeparts"""
import pytest

from gstlal import gstpipetools
from gstlal.pipeparts import pipetools, trigger
from gstlal.pipeparts.pipetools import Gst, GObject
from gstlal.utilities import testtools


def assert_elem_props(elem, name):
	assert gstpipetools.is_element(elem)
	assert type(elem).__name__ == name


class TestTrigger:
	"""Group test for trigger elements"""

	@pytest.fixture(scope='class', autouse=True)
	def pipeline(self):
		GObject.MainLoop()
		return Gst.Pipeline('TestEncode')

	@pytest.fixture(scope='class', autouse=True)
	def src(self, pipeline):
		return pipetools.make_element_with_src(pipeline, None, 'fakesrc')

	@testtools.requires_full_build
	def test_trigger(self, pipeline, src):
		"""Test test multi file"""
		elem = trigger.trigger(pipeline, src, 1)
		assert_elem_props(elem, 'GSTLALTrigger')

	@testtools.impl_deprecated
	@testtools.requires_full_build
	def test_burst_trigger_gen(self, pipeline, src):
		"""Test burst gen"""
		elem = trigger.burst_trigger_gen(pipeline, src)
		assert_elem_props(elem, 'GstTrigger')

	@testtools.impl_deprecated
	@testtools.requires_full_build
	def test_blcbc_trigger_gen(self, pipeline, src):
		"""Test blcbc gen"""
		elem = trigger.blcbc_trigger_gen(pipeline, src, 0.0, 'file', 0.0, 0.0)
		assert_elem_props(elem, 'GstTrigger')

	@testtools.impl_deprecated
	@testtools.requires_full_build
	def test_trigger_gen(self, pipeline, src):
		"""Test gen"""
		elem = trigger.trigger_gen(pipeline, src, 0.0, 'file', 0.0, 0.0)
		assert_elem_props(elem, 'GstTrigger')

	@testtools.impl_deprecated
	@testtools.requires_full_build
	def test_itac(self, pipeline, src):
		"""Test """
		elem = trigger.itac(pipeline, src, 1, 'bank')
		assert_elem_props(elem, 'GSTLALDenoiser')
