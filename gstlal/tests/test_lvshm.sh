#!/bin/bash
outdir=`mktemp -d ./lvshm-out.XXX`
if [ ! -d "$outdir" ]; then
    echo "Could not create temporary directory."
    exit 1
fi
echo "Writing files to directory: $outdir"
gst-launch-1.0 gds_lvshmsrc shm-name=R3LLO_Data num-buffers=100 ! framecpp_filesink path=$outdir instrument=L1
