#!/usr/bin/env python3
#
# Copyright (C) 2020  Patrick Godwin (patrick.godwin@ligo.org)
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import argparse

from gstlal.config import Config
from gstlal.dags import DAG
from gstlal.datafind import DataCache, DataType


parser = argparse.ArgumentParser()
parser.add_argument("-c", "--config", help="Sets the path to read configuration from.")

# load config
args = parser.parse_args()
config = Config.load(args.config)
config.setup()
config.create_time_bins(start_pad=0, overlap=0, one_ifo_only=True)

# create dag
dag = DAG(config)
dag.create_log_dir()

# generate dag layers
if config.frames.whiten_type in ("psd", "median_psd"):
	ref_psd = dag.reference_psd()
	if config.frames.whiten_type == "psd":
		ref_psd = dag.smoothen_psd(ref_psd)
	elif config.frames.whiten_type == "median_psd":
		ref_psd = dag.median_psd(ref_psd)
elif config.frames.whiten_type == "file":
	ref_psd = DataCache.from_files(DataType.REFERENCE_PSD, config.frames.reference_psd)

dag.create_frames(ref_psd)

# write dag/script to disk
dag_name = "fake_frames_dag"
dag.write_dag(f"{dag_name}.dag")
dag.write_script(f"{dag_name}.sh")
