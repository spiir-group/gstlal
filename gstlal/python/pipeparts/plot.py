"""Module for producing plotting elements

"""

from gstlal.pipeparts import pipetools


def channelgram(pipeline: pipetools.Pipeline, src: pipetools.Element, **properties: dict) -> pipetools.Element:
	"""Scrolling channel amplitude plot, generates video showing a scrolling plot of channel amplitudes.

	Args:
		pipeline:
			Gst.Pipeline, the pipeline to which the new element will be added
		src:
			Gst.Element, the source element
		**properties:
			dict, keyword arguments to be set as element properties

	Notes:
		Available Properties:
			plot_width:
				float, default 0.0, width of the plot in seconds. (0 = 1/framerate)

	References:
		[1] ChannelGram Implementation: gstlal/gstlal/gst/python/lal_channelgram.py

	Returns:
		Element, the plot element
	"""
	return pipetools.make_element_with_src(pipeline, src, "lal_channelgram", **properties)


def spectrum(pipeline: pipetools.Pipeline, src: pipetools.Element, **properties: dict) -> pipetools.Element:
	"""Power spectrum plot, generates a video showing a power spectrum (e.g., as measured by lal_whiten).

	Args:
		pipeline:
			Gst.Pipeline, the pipeline to which the new element will be added
		src:
			Gst.Element, the source element
		**properties:
			dict, keyword arguments to be set as element properties

	Notes:
		Available properties:
			f_min:
				float, default 10.0, Lower bound of plot in Hz
			f_max:
				float, default 4000.0, Upper bound of plot in Hz

	References:
		[1] SpectrumPlot Implementation: gstlal/gstlal/gst/python/lal_spectrumplot.py

	Returns:
		Element, the plot element
	"""
	return pipetools.make_element_with_src(pipeline, src, "lal_spectrumplot", **properties)


def histogram(pipeline: pipetools.Pipeline, src: pipetools.Element) -> pipetools.Element:
	"""Histogram plot, generates a video showing a histogram of the input time series.

	Args:
		pipeline:
			Gst.Pipeline, the pipeline to which the new element will be added
		src:
			Gst.Element, the source element

	References:
		[1] HistogramPlot Implementation: gstlal/gstlal/gst/python/lal_histogramplot.py

	Returns:
		Element, the plot element
	"""
	return pipetools.make_element_with_src(pipeline, src, "lal_histogramplot")
