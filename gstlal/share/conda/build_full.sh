# Script for building GstLAL from source

# Discover repository paths
SCRIPT_PATH="$(
  cd -- "$(dirname "$0")" >/dev/null 2>&1
  pwd -P
)"
CONDA_PATH=$(
  builtin cd $SCRIPT_PATH
  pwd
)

source $CONDA_PATH/set_env.sh

# Define GNU sequence function
function install_sequence() {
  make clean

  echo "Running init"
  clear
  ./00init.sh
  sleep 3

  echo "Running configure"
  clear
  ./configure --prefix=$DEPS_PATH
  sleep 3

  echo "Running make"
  clear
  make
  sleep 3

  echo "Running make install"
  clear
  make install
  echo Done
  sleep 3
}

# Move to gstlal location
cd $REPO_PATH/gstlal
echo "Installing: gstlal"
install_sequence

# Move to gstlal location
cd $REPO_PATH/gstlal-ugly
echo "Installing: gstlal-ugly"
install_sequence

# Move to gstlal location
cd $REPO_PATH/gstlal-burst
echo "Installing: gstlal-burst"
install_sequence

# Move to gstlal location
cd $REPO_PATH/gstlal-inspiral
echo "Installing: gstlal-inspiral"
install_sequence
