//
// GDS LIGO-Virgo /dev/shm frame file source element
//
// Copyright (C) Ron Tapia, The Pennsylvania State University 2022
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//
// SECTION:devshmsrc
// @short_description: LIGO-Virgo /dev/shm frame file source element.
//
// Reviewed: NOT REVIEWED
//
// Actions:
// - Review
//

//
// C/OS Library includes
//
#include <sys/inotify.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <poll.h>

//
// gds includes
//
#include <gds/lsmp_con.hh>
#include <gds/tconv.h>
#include <gds/SysError.hh>
//
// gstlal includes
//
#include <gstlal/gstlal_debug.h>
#include <devshmsrc.h>

#define URI_SCHEME "devshm"

//
// Declare utility functions
//
static ssize_t r_read (int fd, void *buf, size_t size);
static void free_queue_element (void *data, void *userData);
static GstFlowReturn devshm_fill (GstBaseSrc *basesrc, guint64 offset, GstBuffer **bufferpp);
static GstFlowReturn devshm_open_next_file (GstBaseSrc *basesrc);
static GstFlowReturn devshm_get_filenames (GstBaseSrc *basesrc);
static int devshm_timestamp_for_file(const char *filename, GstClockTime *timestamp);
static int devshm_is_fake(const char *filename);

//
//
//
static GstURIType uri_get_type(GType type)
{
  return GST_URI_SRC;
}

static const gchar *const *uri_get_protocols(GType type)
{
  static const gchar *protocols[] = {URI_SCHEME, NULL};
  return protocols;
}

static gchar *uri_get_uri(GstURIHandler *handler)
{
  GDSDEVSHMSrc *element = GDS_DEVSHMSRC(handler);
  return g_strdup_printf(URI_SCHEME ":%s", element->name);
}

static gboolean uri_set_uri(GstURIHandler *handler, const gchar *uri, GError **err)
{
  GDSDEVSHMSrc *element = GDS_DEVSHMSRC(handler);
  gchar *scheme = g_uri_parse_scheme(uri);
  gchar name[strlen(uri)];
  gboolean success = TRUE;

  success = !strcmp(scheme, URI_SCHEME);
  if(success)
    success &= sscanf(uri, URI_SCHEME ":%s", name) == 1;
  if(success) {
    if (name[strlen(name)] == '/') {
      name[strlen(name)] = 0;
    }
    g_object_set(G_OBJECT(element), "shm-dir", name, NULL);
  }
  g_free(scheme);
  return success;
}

static void uri_handler_init(gpointer g_iface, gpointer iface_data)
{
  GstURIHandlerInterface *iface = (GstURIHandlerInterface *) g_iface;

  iface->get_uri = GST_DEBUG_FUNCPTR(uri_get_uri);
  iface->set_uri = GST_DEBUG_FUNCPTR(uri_set_uri);
  iface->get_type = GST_DEBUG_FUNCPTR(uri_get_type);
  iface->get_protocols = GST_DEBUG_FUNCPTR(uri_get_protocols);
}

#define GST_CAT_DEFAULT gds_devshmsrc_debug
GST_DEBUG_CATEGORY_STATIC(GST_CAT_DEFAULT);

static void additional_initializations(GType type)
{
  static const GInterfaceInfo uri_handler_info = {
    uri_handler_init,
    NULL,
    NULL
  };
  g_type_add_interface_static(type, GST_TYPE_URI_HANDLER, &uri_handler_info);

  GST_DEBUG_CATEGORY_INIT(GST_CAT_DEFAULT, "gds_devshmsrc", 0, "gds_devshmsrc element");
}

G_DEFINE_TYPE_WITH_CODE(GDSDEVSHMSrc,
                        gds_devshmsrc,
                        GST_TYPE_PUSH_SRC,
                        additional_initializations(g_define_type_id));

//
// Paremeters
//
#define DEFAULT_SHM_DIRNAME NULL
#define DEFAULT_MASK -1             //
#define DEFAULT_WAIT_TIME 120      // Wait for 2 minutes when polling inotify fd.
#define DEFAULT_WATCH_SUFFIX NULL // Watch for all files

//
// Utilities
//
static GstClockTime GPSNow(void)
{
  // FIXME:  why does TAInow() return the GPS time? Is this necessary?
  // TAI is "Temps Atomique International" or "international atomic time".
  // http://leapsecond.com/java/gpsclock.htm
  // TAInow is defined in gds/Base/time/tconv.c
  // This return value of TAInow appears to be in nanoseconds.
  // Is gst_util_uint64_scale_int_round necessary?
  // It certainly seems odd that we use a function named TAInow,
  // scale its return value by 1, and return it from a function named GPSNow.
  return gst_util_uint64_scale_int_round(TAInow(), GST_SECOND, _ONESEC);
}

//
// Handle request to start processing.
//
static gboolean start(GstBaseSrc *object)
{
  GDSDEVSHMSrc *element = GDS_DEVSHMSRC(object);
  gboolean success = TRUE;
  int status;

  element->currentFd = -1;
  element->next_timestamp = 0;

  if(!element->name) {
    GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("shm-dir not set"));
    success = FALSE;
    goto done;
  }

  element->notifyFd = inotify_init1(IN_NONBLOCK);

  if (element->notifyFd == -1) {
    GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("could not initialize inotify for shm-dir: \"%s\"", element->name));
    success = FALSE;
    goto done;
  }
  status = inotify_add_watch(element->notifyFd, element->name, IN_ONLYDIR|IN_CLOSE_WRITE|IN_MOVED_TO);
  if (status == -1) {
    GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("could not add watch on shm-dir: \"%s\" : %s", element->name, strerror(errno)));
    success = FALSE;
    goto done;
  }
  element->inotifyWatchDescriptor = status;
  GST_DEBUG_OBJECT(element, "watching directory: \"%s\"", element->name);

  // If g_malloc fails, it takes the program down, so no need to check return value.
  element->inotifyEventLen = DEVSHM_MAX_FILE_EVENTS * (sizeof(struct inotify_event) + NAME_MAX + 1);
  element->inotifyEventp = (char *) g_malloc(element->inotifyEventLen);
  GST_DEBUG_OBJECT(element, "allocated memory for inotify events");

  // Can g_queue_new() fail...I mean without taking down the program because of a g_malloc?
  element->filenameQueue = g_queue_new();
  GST_DEBUG_OBJECT(element, "created filenameQueue");

  element->currentFullPath = g_string_sized_new(PATH_MAX + 1);

  element->lastCreatedBufferTimestamp = 0;
  
 done:
  GST_DEBUG_OBJECT(element, "start complete, return value: %d", success);
  return success;
}

//
// Handle request to stop processing.
//
static gboolean stop(GstBaseSrc *object)
{
  GDSDEVSHMSrc *element = GDS_DEVSHMSRC(object);

  // Ignore errors, but emit a warning if we run into
  // problems here.
  try {
    close(element->notifyFd);
    g_free(element->inotifyEventp);
    g_queue_foreach(element->filenameQueue, &free_queue_element, NULL);
    g_queue_free(element->filenameQueue);
    g_string_free(element->currentFullPath, TRUE);
  } catch (const SysError & e) {
    GST_WARNING_OBJECT(element, "devshmsrc:stop Caught SysError description: [%s]", e.what());
  } catch (const std::exception & e) {
    GST_WARNING_OBJECT(element, "devshmsrc:stop Caught error with description: [%s]", e.what());
  }
  return TRUE;
}

static gboolean unlock(GstBaseSrc *basesrc)
{
  GDSDEVSHMSrc *element = GDS_DEVSHMSRC(basesrc);
  gboolean success = TRUE;

  element->unblocked = TRUE;
  if(!g_mutex_trylock(&element->create_thread_lock))
    success = !pthread_kill(element->create_thread, SIGALRM);
  else
    g_mutex_unlock(&element->create_thread_lock);

  return success;
}

static gboolean unlock_stop(GstBaseSrc *basesrc)
{
  GDSDEVSHMSrc *element = GDS_DEVSHMSRC(basesrc);
  gboolean success = TRUE;

  element->unblocked = FALSE;

  return success;
}

//
// Handle a request to create a buffer. Ignore offset and size.
//
static GstFlowReturn create(GstBaseSrc *basesrc, guint64 offset, guint size, GstBuffer **bufferpp)
{
  GDSDEVSHMSrc  *element = GDS_DEVSHMSRC(basesrc);
  GstFlowReturn  result  = GST_FLOW_OK;

  *bufferpp = NULL;

  // application thread kill
  // typically from a sig 15 kill
  if(element->unblocked) {
      GST_DEBUG_OBJECT(element, "unlock() called, no buffer created");
      return GST_FLOW_EOS;
  }

  // Can create be called in multiple threads concurrently?
  // If so, these locks are necessary. If not, they
  // are, like the inhabitants of earth, mostly harmless.
  element->create_thread = pthread_self();
  g_mutex_lock(&element->create_thread_lock);
  result = devshm_fill(basesrc, offset, bufferpp);
  g_mutex_unlock(&element->create_thread_lock);

  return result;
}

//
// Given a pointer to a pointer to a GstBuffer, create a GstBuffer (sized appropriately)
// that holds the contents of the next frame file in the queue.
//
static GstFlowReturn devshm_fill (GstBaseSrc *basesrc, guint64 offset, GstBuffer **bufferpp)
{
  GstBaseSrcClass *basesrc_class = GST_BASE_SRC_CLASS(G_OBJECT_GET_CLASS(basesrc));
  GDSDEVSHMSrc    *element = GDS_DEVSHMSRC(basesrc);
  GstFlowReturn    result = GST_FLOW_OK;
  GstMapInfo       mapinfo;
  size_t           bytesRead = 0;
  ssize_t          readResult;

  result = devshm_open_next_file(basesrc);
  if (result != GST_FLOW_OK) {
    goto done;
  }

  result = basesrc_class->alloc(basesrc, 0, element->currentFileSize, bufferpp);
  if(result != GST_FLOW_OK) {
    GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("alloc() returned %d (%s)", result, gst_flow_get_name(result)));
    goto done;
  }

  gst_buffer_map(*bufferpp, &mapinfo, GST_MAP_WRITE);

  if(!(element->currentFileSize==bytesRead)){
    do {
      readResult = r_read(element->currentFd, mapinfo.data + bytesRead, mapinfo.size - bytesRead);
      if (readResult < 0) {
        result = GST_FLOW_ERROR;
        GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("read('%s') failed: %s", element->currentFilename, strerror(errno)));
        gst_buffer_unmap(*bufferpp, &mapinfo);
        gst_buffer_unref(*bufferpp);
        *bufferpp = NULL;
        goto done;
      } else {
        bytesRead += readResult;
      }
    } while (bytesRead < element->currentFileSize);
  }
  g_assert_cmpuint(bytesRead, ==, element->currentFileSize);

  GST_DEBUG_OBJECT(element, "Just before buffer timestamp setting. Current timestamp: %ld", element->currentFileTimestamp);
  GST_BUFFER_PTS(*bufferpp)        = element->currentFileTimestamp * GST_SECOND;
  GST_BUFFER_OFFSET(*bufferpp)     = offset;
  GST_BUFFER_OFFSET_END(*bufferpp) = GST_BUFFER_OFFSET_NONE;

  // If current buffer more than 1 sec (1 buffer length) away from last, set discontinuity flag.
  if (element->lastCreatedBufferTimestamp && (element->currentFileTimestamp - element->lastCreatedBufferTimestamp) > 1) {
    GST_BUFFER_FLAG_SET(*bufferpp, GST_BUFFER_FLAG_DISCONT);
    GST_WARNING_OBJECT(element, "discontinuity @ %" GST_TIME_SECONDS_FORMAT, GST_TIME_SECONDS_ARGS(GST_BUFFER_PTS(*bufferpp)));
  }

  // If pushing fake buffers, mark with gap flag
  if (element->currentFileSize==0){
      GST_BUFFER_DURATION(*bufferpp) = 0;
      element->next_timestamp        = GST_BUFFER_PTS(*bufferpp) + GST_SECOND;
      GST_BUFFER_FLAG_SET(*bufferpp, GST_BUFFER_FLAG_GAP);
      GST_WARNING_OBJECT(element, "gap data @ %" GST_TIME_SECONDS_FORMAT, GST_TIME_SECONDS_ARGS(GST_BUFFER_PTS(*bufferpp)));
  } else{
      GST_BUFFER_DURATION(*bufferpp) = GST_SECOND;
      element->next_timestamp        = GST_BUFFER_PTS(*bufferpp) + GST_BUFFER_DURATION(*bufferpp);
  }

  element->max_latency    = GPSNow() - GST_BUFFER_PTS(*bufferpp);
  element->min_latency    = element->max_latency - GST_BUFFER_DURATION(*bufferpp);    
  GST_DEBUG_OBJECT(element, "max_latency: %ld min_latency: %ld  shm-dirname: \"%s\" buffer timestamp: %ld next timestamp: %ld, GPSNow(): %ld, offset: %ld", element->max_latency, element->min_latency, element->name, GST_BUFFER_PTS(*bufferpp), element->next_timestamp, GPSNow(), offset);

  close(element->currentFd);
  gst_buffer_unmap(*bufferpp, &mapinfo);

  element->currentFd = -1;
  element->lastCreatedBufferTimestamp = element->currentFileTimestamp;

 done:
  return result;
}

//
// If the filename queue is empty, fill it.
//
// Open the next filename on the filename queue if not fake.
//
// If the next filename on the queue is fake data according
// to devshm_is_fake(file), then don't try to open it. Instead
// set element->currentFileSize to zero.
//
static GstFlowReturn devshm_open_next_file (GstBaseSrc *basesrc)
{
  GDSDEVSHMSrc  *element = GDS_DEVSHMSRC(basesrc);
  GstFlowReturn  result  = GST_FLOW_OK;
  GString       *nextFilenameGstringp;
  int            status;
  struct stat    statinfo;

  while (g_queue_get_length(element->filenameQueue) == 0) {
    result = devshm_get_filenames(basesrc);
    if (result != GST_FLOW_OK) {
      GST_DEBUG_OBJECT(element, "devshm_get_filenames returned error: \"%s\"", element->name);
      goto done;
    }
  }

  // Pop item off the queue and free as soon as we're done using it.
  nextFilenameGstringp = (GString *) g_queue_pop_head(element->filenameQueue);
  strncpy(element->currentFilename, nextFilenameGstringp->str, nextFilenameGstringp->len + 1);
  GST_DEBUG_OBJECT(element, "new filename: \"%s\"", element->currentFilename);
  g_string_free(nextFilenameGstringp, TRUE);

  // Set element->currentFileTimestamp to the timestamp in file name.
  if (devshm_timestamp_for_file(element->currentFilename, &(element->currentFileTimestamp)) != 0) {
    GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("No timestamp in filename: '%s'", element->currentFilename));
    result = GST_FLOW_ERROR;
    goto done;
  }
  
  // Check if filename corresponds to fake data name. If so, skip opening file.
  if (devshm_is_fake(element->currentFilename) == 1){
    GST_DEBUG_OBJECT(element, "no data filename for \"%s\". Setting current file size to 0, timestamp: %ld", element->currentFilename, element->currentFileTimestamp);
    element->currentFileSize = size_t(0);
    goto done;
  }

  g_string_printf(element->currentFullPath, "%s/%s", element->name, element->currentFilename);
  element->currentFd = open(element->currentFullPath->str, O_RDONLY);

  // Should an error opening the next file be a GST_FLOW_ERROR?
  // Maybe we are running behind and the file was deleted.
  // That would put us at 5 minutes behind which is worrisome.
  // For now, consider it an error.
  if (element->currentFd == -1) {
    GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("failed to open file: '%s' : %s", element->currentFilename, strerror(errno)));
    result = GST_FLOW_ERROR;
    goto done;
  }
  GST_DEBUG_OBJECT(element, "opened file: \"%s\"", element->currentFullPath->str);

  // Find the size of the current file.
  status = fstat(element->currentFd, &statinfo);
  if (status != 0) {
    GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("fstat('%s') failed: %s", element->currentFilename, strerror(errno)));
    result = GST_FLOW_ERROR;
    close(element->currentFd);
    goto done;
  }
  element->currentFileSize = statinfo.st_size;
  GST_DEBUG_OBJECT(element, "size of file: \"%s\" is %ld bytes", element->currentFilename, statinfo.st_size);

 done:
  return result;
}

//
// Poll the notification fd. If we timeout, create a fake
// no-data filename. Later on in the processing it will
// be interpreted and a zero-length heartbeat buffer will
// be produced.
//
static GstFlowReturn devshm_get_filenames (GstBaseSrc *basesrc)
{
  GDSDEVSHMSrc  *element = GDS_DEVSHMSRC(basesrc);
  GstFlowReturn  result  = GST_FLOW_OK;
  ssize_t        readResult = 0;
  size_t         parsedEventsLen = 0;
  GString       *filenameGstringp;
  int            count = 0;
  int            numFilenamesAdded = 0;
  int            pollret;
  struct pollfd  pfd;
  timeval        startTv, endTv;
  double         elapsedSeconds;
  struct inotify_event *eventp;
  GstClockTime startPoll;
  char*         suffixStart;
  size_t        eventNameLen;

  // Poll the notify file descriptor.
  // The timeout is set in datasource.py.
  // If we are not seeing files,
  // push empty 0 length buffers.
  pfd.fd     = element->notifyFd;
  pfd.events = POLLIN;
  startPoll = GPSNow();
  gettimeofday(&startTv, NULL);
  pollret = poll(&pfd, 1, element->wait_time);
  gettimeofday(&endTv, NULL);
  elapsedSeconds = endTv.tv_sec - startTv.tv_sec;

  // if our poll returned events, and there is data to read, read it and store in inotifyEventp
  if ((pollret > 0 ) && (pfd.revents & POLLIN)) {
    readResult = r_read(element->notifyFd, element->inotifyEventp, element->inotifyEventLen);
  } else {
    // timeout, there is no new data available.
    // Create and push filenames with timestamps of missing times

    GstClockTime fakeTimestamp = 0; // Timestamp of the fake file that we are generating.

    // If we are here and there and we have never added a filename to the queue,
    // pretend that we did when we started polling.
    if(!element->lastFileTimestamp) {
            element->lastFileTimestamp = (startPoll / GST_SECOND) - 1;
    }
    g_assert_cmpfloat(elapsedSeconds, >, 0.0);

    int tm;
    for (tm = 0; tm < elapsedSeconds; tm++) {
      // make fake filename to hold empty buffer place
      filenameGstringp = g_string_new(NULL);
      fakeTimestamp = element->lastFileTimestamp + 1; // assumes buffers are 1s
      g_string_printf(filenameGstringp, "no-data-%ld-1.gwf", fakeTimestamp);
      // add the string to the filenameQueue
      g_queue_push_tail(element->filenameQueue, (gpointer) filenameGstringp);
      element->lastFileTimestamp = fakeTimestamp; // Keep track of the timestamp of the last file added to the queue.       
      GST_DEBUG_OBJECT(element, "Adding no-data filename at time %ld", fakeTimestamp);
    }
    GST_DEBUG_OBJECT(element, "Timeout occured. Added %d no-data filenames", tm);
    goto done;
  }

  if (readResult == -1) {
    if (errno == EAGAIN) {
      GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("Read returned EAGAIN after successful poll (very odd): %s", strerror(errno)));
      result = GST_FLOW_ERROR;
      goto done;
    }
    GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("Failed to read notifyFd: %s", strerror(errno)));
    result = GST_FLOW_ERROR;
    goto done;
  }
  GST_DEBUG_OBJECT(element, "Read %ld bytes from inotify fd: \"%s\"", readResult, element->name);

  // fetch the buffer/event from the element property
  eventp = (struct inotify_event *) element->inotifyEventp;

  // if space of events read is less that the size of available available events, read more.
  GstClockTime fileTimestamp;
  while (parsedEventsLen < (size_t) readResult) {
    GST_DEBUG_OBJECT(element, "Parsing inotify event %d (size: %ld + %d) in \"%s\"", count, sizeof(struct inotify_event), eventp->len, element->name);
    g_assert_cmpint(eventp->wd, ==, element->inotifyWatchDescriptor);
    // if the filename in event has zero length, fail.
    if (eventp->len == 0) {
      GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("Zero length filename in file create notification event"));
      result = GST_FLOW_ERROR;
      goto done;
    } else if (eventp->len > NAME_MAX) { //if the name is too long, error
      GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("Filename longer than NAME_MAX in file create notification event"));      
      result = GST_FLOW_ERROR;
      goto done;
    }

    // If watch_suffix is set, test the suffix and ignore
    // the event if the suffix does not match.
    if (element->watch_suffix){
      eventNameLen = strlen(eventp->name);
      if (eventNameLen < element->watch_suffix_len) goto next_event;
      suffixStart = eventp->name + eventNameLen - element->watch_suffix_len;
      if (g_ascii_strncasecmp(element->watch_suffix, suffixStart, element->watch_suffix_len)) {
	GST_DEBUG_OBJECT(element, "Filename does not match watch_suffix. Filename: \"%s\", Suffix:\"%s\", Desired Suffix: \"%s\"", element->name,
			 suffixStart, element->watch_suffix);
	goto next_event;
      }
    }

    // after checks, turn the event name into a filename string.
    filenameGstringp = g_string_new(eventp->name);

    // Fetch timestamp for filename.
    if (devshm_timestamp_for_file(filenameGstringp->str, &fileTimestamp) != 0) {
      GST_ELEMENT_ERROR(element, RESOURCE, READ, (NULL), ("No timestamp in filename: '%s'", filenameGstringp->str));
      result = GST_FLOW_ERROR;
      goto done;
    }

    // Check that timestamp is greater than last timestamp pushed to the queue.
    // If data comes late, don't want to push a file that we previously pushed an empty buffer for.
    // Under normal operation. The assumption that inotify events are ordered in time is good.
    if (fileTimestamp > element->lastFileTimestamp){
      g_queue_push_tail(element->filenameQueue, (gpointer) filenameGstringp); // Add the string to the filenameQueue
      numFilenamesAdded++;
      element->lastFileTimestamp = fileTimestamp; // Keep track of the timestamp of the last file added to the queue.       
    } else{
      g_string_free(filenameGstringp, TRUE);
      GST_DEBUG_OBJECT(element, "Time reversal, skipping data at %ld", fileTimestamp);
    }

    // Add this event/buffer to the total parsed length.
  next_event:
    parsedEventsLen += sizeof(struct inotify_event) + eventp->len;
    eventp = (struct inotify_event *) (element->inotifyEventp + parsedEventsLen);
    count++;
  }

  GST_DEBUG_OBJECT(element, "Found %d new files (%d events) in \"%s\"", numFilenamesAdded, count, element->name);

 done:
  return result;
}

static gboolean query(GstBaseSrc *basesrc, GstQuery *query)
{
  GDSDEVSHMSrc *element = GDS_DEVSHMSRC(basesrc);
  gboolean success = TRUE;
  switch(GST_QUERY_TYPE(query)) {
  case GST_QUERY_FORMATS:
    gst_query_set_formats(query, 1, GST_FORMAT_TIME);
    break;
  case GST_QUERY_LATENCY:
    gst_query_set_latency(query, gst_base_src_is_live(basesrc), element->min_latency, element->max_latency);
    break;
  case GST_QUERY_POSITION:
    // timestamp of next buffer
    gst_query_set_position(query, GST_FORMAT_TIME, element->next_timestamp);
    break;
  default:
    success = GST_BASE_SRC_CLASS(gds_devshmsrc_parent_class)->query(basesrc, query);
    break;
  }
  if(success)
    GST_DEBUG_OBJECT(element, "query result: %" GST_PTR_FORMAT, query);
  else
    GST_ERROR_OBJECT(element, "query failed");

  return success;
}

//
// Version of read that handles being interrupted by a signal.
//
static ssize_t r_read (int fd, void *buf, size_t size)
{
  ssize_t retval;
  while (retval = read(fd, buf, size), retval == -1 && errno == EINTR) ;
  return retval;
}

//
// Utility function for freeing our filename queue elements (which are all GStrings).
//
static void free_queue_element (void *data, void *userData)
{
  g_string_free((GString *) data, TRUE);
}

//
// Find the timestamp in a frame format filename. The
// format of the filename is expected to be:
//
//    FOO-FOO-TIMESTAMP-1.gwf
//
// The timestamp returned is in GPS seconds.
//
// Note: Based on: https://dcc.ligo.org/LIGO-T010150
//       the last number of the file name is not necessarily the duration
//       of the frame data in the file. It works out this way for
//       the file processed out of /dev/shm, but it need not be
//       the case in general.
//
static int devshm_timestamp_for_file(const char *filename, GstClockTime *timestamp) 
{
  const char *ts_start = 0;
  uint  i;
  uint  count = 0;
  long  retval = 0;

  for (i=0; i< strlen(filename); i++) {
    if (*(filename + i) == '-') {
      count++;
    }
    if (count == 2) {
      ts_start = filename + i + 1;
      break;
    }
  }

  if (ts_start == 0) {
    retval = -1;
    goto done;
  }

  if (sscanf(ts_start, "%ld-1.gwf", timestamp) != 1) {
    retval = -1;
    goto done;
  }

 done:
  return retval;
}

// Utility function to check for bogus filenames created when no data available.
// Return 1 = fake file
static int devshm_is_fake(const char *filename)
{
  long  retval = 0;

  if (!strncmp(filename, "no-data-xx", 7)) {
    retval = 1;
    goto done;
  }

 done:
  return retval;
}

//
// properties
//
enum property {
  ARG_SHM_DIRNAME = 1,
  ARG_MASK,
  ARG_WAIT_TIME,
  ARG_WATCH_SUFFIX,
};

static void set_property(GObject *object, guint id, const GValue *value, GParamSpec *pspec)
{
  GDSDEVSHMSrc *element = GDS_DEVSHMSRC(object);

  GST_OBJECT_LOCK(element);

  switch((enum property) id) {
  case ARG_SHM_DIRNAME:
    g_free(element->name);
    element->name = g_value_dup_string(value);
    break;

  case ARG_MASK:
    element->mask = g_value_get_uint(value);
    break;

  case ARG_WAIT_TIME:
    element->wait_time = g_value_get_double(value) * 1000;
    break;

  case ARG_WATCH_SUFFIX:
    g_free(element->watch_suffix);
    element->watch_suffix = g_value_dup_string(value);
    if (element->watch_suffix != NULL) {
      element->watch_suffix_len = strlen(element->watch_suffix);
    }
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, id, pspec);
    break;
  }

  GST_OBJECT_UNLOCK(element);
}

static void get_property(GObject *object, guint id, GValue *value, GParamSpec *pspec)
{
  GDSDEVSHMSrc *element = GDS_DEVSHMSRC(object);

  GST_OBJECT_LOCK(element);

  switch((enum property) id) {
  case ARG_SHM_DIRNAME:
    g_value_set_string(value, element->name);
    break;

  case ARG_MASK:
    g_value_set_uint(value, element->mask);
    break;

  case ARG_WAIT_TIME:
    g_value_set_double(value, element->wait_time);
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object, id, pspec);
    break;
  }

  GST_OBJECT_UNLOCK(element);
}

static void finalize(GObject *object)
{
  GDSDEVSHMSrc *element = GDS_DEVSHMSRC(object);

  g_free(element->name);
  element->name = NULL;
  g_free(element->watch_suffix);
  element->watch_suffix = NULL;
  g_mutex_clear(&element->create_thread_lock);

  G_OBJECT_CLASS(gds_devshmsrc_parent_class)->finalize(object);
}


static void gds_devshmsrc_class_init(GDSDEVSHMSrcClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
  GstElementClass *element_class = GST_ELEMENT_CLASS(klass);
  GstBaseSrcClass *gstbasesrc_class = GST_BASE_SRC_CLASS(klass);

  gobject_class->set_property = GST_DEBUG_FUNCPTR(set_property);
  gobject_class->get_property = GST_DEBUG_FUNCPTR(get_property);
  gobject_class->finalize = GST_DEBUG_FUNCPTR(finalize);

  gstbasesrc_class->start = GST_DEBUG_FUNCPTR(start);
  gstbasesrc_class->stop = GST_DEBUG_FUNCPTR(stop);
  gstbasesrc_class->unlock = GST_DEBUG_FUNCPTR(unlock);
  gstbasesrc_class->unlock_stop = GST_DEBUG_FUNCPTR(unlock_stop);
  gstbasesrc_class->create = GST_DEBUG_FUNCPTR(create);
  gstbasesrc_class->query = GST_DEBUG_FUNCPTR(query);

  gst_element_class_set_details_simple(element_class,
                                       "GDS DEVSHM Frame File Source",
                                       "Source",
                                       "LIGO-Virgo /dev/shm frame file source element",
                                       "Ron Tapia <ron.tapia@ligo.org>"
                                       );

  gst_element_class_add_pad_template(element_class,
                                     gst_pad_template_new("src",
                                                          GST_PAD_SRC,
                                                          GST_PAD_ALWAYS,
                                                          gst_caps_from_string("application/x-igwd-frame, " "framed = (boolean) true"))
                                     );

  g_object_class_install_property(
                                  gobject_class,
                                  ARG_SHM_DIRNAME,
                                  g_param_spec_string("shm-dirname",
                                                      "Name",
                                                      "Shared memory directory name.  Suggestions:  \"/dev/shm/kafka/L1_O3E2ERep1\", \"/dev/shm/kafka/H1_O3E2ERep1\", \"/dev/shm/kafka/V1_O3E2ERep1\".",
                                                      DEFAULT_SHM_DIRNAME,
                                                      (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT))
                                  );

  g_object_class_install_property(gobject_class,
                                  ARG_MASK,
                                  g_param_spec_uint("mask",
                                                    "Trigger mask",
                                                    "Buffers will be received only from producers whose masks' bit-wise logical and's with this value are non-zero.",
                                                    0, G_MAXUINT, DEFAULT_MASK,
                                                    (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT))
                                  );

  g_object_class_install_property(
                                  gobject_class,
                                  ARG_WAIT_TIME,
                                  g_param_spec_double("wait-time",
                                                      "Wait time",
                                                      "Wait time in seconds (<0 = wait indefinitely, 0 = never wait).",
                                                      -G_MAXDOUBLE, G_MAXDOUBLE, DEFAULT_WAIT_TIME,
                                                      (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT))
                                  );

  g_object_class_install_property(
                                  gobject_class,
                                  ARG_WATCH_SUFFIX,
                                  g_param_spec_string("watch-suffix",
                                                      "Watch suffix",
                                                      "Suffix of filenames to watch for in polling. Default is to watch all files regardless of suffix. Suggestions:  \".gwf\", \".h5\", etc",
                                                      DEFAULT_WATCH_SUFFIX,
                                                      (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT))
                                  );

}

static void gds_devshmsrc_init(GDSDEVSHMSrc *element)
{
  GstBaseSrc *basesrc = GST_BASE_SRC(element);

  gst_base_src_set_live(basesrc, TRUE);
  gst_base_src_set_format(basesrc, GST_FORMAT_TIME);

  element->name = NULL;
  element->watch_suffix = NULL;
  element->max_latency = element->min_latency = GST_CLOCK_TIME_NONE;
  element->unblocked = FALSE;
  g_mutex_init(&element->create_thread_lock);
  element->partition = NULL;
}
